# PUC Calculadora Web

Curso: Gerenciamento de Código e Controle de Versão

Autor: Juliano Santos

## Projeto de Calculadora Web

Esse projeto é uma aplicação Web (html/css/js) de uma calculadora.

Ele é executado direto no browser.

### Arquivos do Projeto

- calculadora.html : arquivo principal, tela da calculadora.
- css/stiles.css : arquivo de configurações visuais.
- js/calculos.js : arquivo com os métodos que executam os calculos

